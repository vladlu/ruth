#![deny(clippy::all)]
use std::{collections::{LinkedList as lis, HashMap}, env::args, fmt::format};

#[cfg(feature = "interpreter")]
mod interpreter;

#[cfg(feature = "llvm")]
pub mod llvmint;

#[derive(Debug)]
pub enum Errors {
    Syntax(Token),
    Parse((usize,usize)),
    StringClosing((usize,usize)),
    ParenUp(i32),
    ParenDown(i32),
    Null,
}

#[derive(PartialEq, Debug, Clone)]
pub struct Func {
    pos_args: Vec<Properties>,
    args: HashMap<String, Token>,
    body: lis<Token>,
    scope: HashMap<String, HashMap<String,Token>>,
}

impl Func {
    fn new(pos_args: Vec<Properties>, body: lis<Token>) -> Self {
        Self {
            pos_args,
            args: HashMap::new(),
            body,
            scope: HashMap::new(),
        }
    } 
    fn null() -> Self {
        Self::new(Vec::new(), lis::new())
    }  
}

#[derive(PartialEq, Clone,Debug)]
pub enum Kind {
    Bool(u64),
    Int(i64),        
    UInt(u64),       
    Float(f64),      
    Str(String),     
    List(lis<Token>),
    Symbol(String),  
    Property(String), // this whole system of properties is TODO
    Func(Func),
    Define,
    DoNotEval,
    RParen,          
    LParen,          
    Space,
    OpPlus,
    OpMinus,
    OpMul,
    OpDiv,
    OpPrint,
    OpTailRec,
    BoolToLambdaRepresentation
}

#[derive(PartialEq,Clone,Debug)]
pub enum Properties {
    Type(String,Kind),
}

impl Properties {
    fn null() -> Self {
        Self::Type("".to_owned(), Kind::Space)
    }
}

#[derive(PartialEq)]
#[derive(Clone,Debug)]
pub struct Token {
    val: Kind,
    loc: (usize,usize), // (chr,line)
}

impl Token {
    fn new(val: Kind, loc: (usize,usize)) -> Self {
        Self {
            val,
            loc
        }
    }
    fn null() -> Self {
        Self::new(Kind::Space, (0,0))
    }
}

impl std::fmt::Display for Func {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut argstr = "Args: ".to_string();
                let bodystr = format!("{}", Kind::List(self.body.clone()));
                for elem in &self.pos_args {
                    argstr = format!("{} => {}, ", argstr, elem);
                }
               let finalstr = format!("{} && Body: {}", argstr, bodystr);
               write!(f,"{}", finalstr)
    }
}

impl std::fmt::Display for Properties {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Properties::Type(a,b) => write!(f,"{}:{}",a,b),
        }
    }
}

impl Kind {
    fn sym_unwrap(self) -> Option<String> {
        match self {
            Kind::Symbol(a) => Some(a),
            _ => None
        }
    }
}

impl std::fmt::Display for Kind {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Kind::Bool(b) => {
                match b {
                    0 => write!(f,"B:false"),
                    _ => write!(f,"B:true"),
                }
            }
            Kind::Int(i) => {
                write!(f, "Int({})", i)
            }
            Kind::Float(n) => {
                write!(f, "Float({})", n)
            }
            Kind::LParen => {
                write!(f, "(")
            }
            Kind::List(l) => { 
                let mut val = String::new();
             for elem in l {
                val = format!("{} [{}] =>", val, elem);
             }
             write!(f,"{}", val)   
            }
            Kind::Property(p) => {
                write!(f,"is {}", &p[1..])
            }
            Kind::RParen => {
                write!(f, ")")
            }
            Kind::Space => {
                write!(f, " ")
            }
            Kind::Str(s) => {
                write!(f, "\"{}\"", s)
            }
            Kind::Symbol(s) => {
                write!(f, "{}", s)
            }
            Kind::UInt(u) => {
                write!(f,"unsignedInt({})", u)
            }
            Kind::Func(fun) => {
               write!(f, "{}", fun) 
            }
            Kind::DoNotEval => {
                write!(f, "N_EVAL")
            }
            Kind::Define => {
                write!(f, "INTRINSIC_Define")
            }
            Kind::OpPlus => {
                write!(f, "OP_+")
            }
            Kind::OpMinus => {
                write!(f, "OP_-")
            }
            Kind::OpDiv => {
                write!(f, "OP_/")
            }
            Kind::OpMul => {
                write!(f, "OP_*")
            }
            Kind::OpPrint => {
                write!(f, "OP_PRINT")
            }
            Kind::OpTailRec => {
                write!(f, "TAIL_RECURSION")
            }
            Kind::BoolToLambdaRepresentation => {
                write!(f, "Eval Bool as Lambda")
            }
        }
    }
}

impl std::fmt::Display for Token {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}, Ln {}, Col {}", self.val, self.loc.1, self.loc.0)
    }
}

impl std::fmt::Display for Errors {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Errors::Parse(loc) => {
                write!(f, "Parse Error: Failed to parse properly at ln {} col {}", loc.1, loc.0)
            }
            Errors::Syntax(t) => {
                write!(f, "Syntax Error: Usage error of \"{}\" @ ln {} col {}", t.val, t.loc.1, t.loc.0)
            }
            Errors::StringClosing(loc) => {
                write!(f, "String Closing Error: String that starts @ ln {} col {} is never closed.", loc.1, loc.0)
            }
            Errors::ParenUp(e) => {
                write!(f, "Parenthesis Count Error: You have an excess of {} parenthesis.", -e)
            }
            Errors::ParenDown(e) => {
                write!(f, "Parenthesis Count Error: You have {} parenthesis missing.", e)
            }
            Errors::Null => {
                write!(f, "Null Error: Internal interpreter error.")
            }
        }
    }
}

pub fn find_coord(line_lens: &Vec<usize>, pos: &usize) -> (usize,usize) {
    let mut line = 1;
    let mut comp_val = line_lens[line - 1];
    let x;
    loop {
        if pos <= &comp_val {
            //println!("cV b: {}", &comp_val);
            comp_val -= line_lens[line - 1];
            //println!("cV a: {}", & comp_val);
            x = pos - comp_val - (line - 1) * 2;
            //println!("p - cV = x ({} - {} = {})", pos, comp_val, x);
            break;
        } else if line < line_lens.len() {
            line += 1;
            comp_val += line_lens[line - 1];
        } else {
            comp_val -= line_lens[line_lens.len() - 1];
            x = pos - comp_val;
            break;
        }
    }
    (x + 1,line)
}


pub fn tokenize(s: String) -> Result<Vec<Token>, Errors> {
    let analyze = s.clone();
    let line_lens: Vec<usize> = analyze.lines().map(|x| x.len()).collect();
    //println!("{:?}", &line_lens);
    let work: Vec<char> = s.chars().collect();
    let work_len = work.len();
    let mut curr_token = Vec::new();
    let mut tokens_arr = Vec::new();
    let mut point_pos = 0usize;
    loop {
        if point_pos >= work_len {
            break;
        }
        match work[point_pos] {
                s if s == '\'' => {
                    let pos = point_pos;
                    let loc = find_coord(&line_lens, &pos);
                    if work[point_pos + 1] == '(' {
                        tokens_arr.push(Token::new(Kind::LParen, loc));
                        tokens_arr.push(Token::new(Kind::DoNotEval, loc));
                        point_pos += 2;
                    } else {
                        return Err(Errors::Syntax(Token::new(Kind::DoNotEval, loc)))
                    }
                }
                s if s == '\\' => {
                    let pos = point_pos;
                    let loc = find_coord(&line_lens, &pos);
                    if work[point_pos + 1] == '(' {
                        tokens_arr.push(Token::new(Kind::LParen, loc));
                        tokens_arr.push(Token::new(Kind::BoolToLambdaRepresentation, loc));
                        point_pos += 2;
                    } else {
                        return Err(Errors::Syntax(Token::new(Kind::BoolToLambdaRepresentation, loc)))
                    }
                }
                n if n.is_numeric() => {
                    let pos = point_pos;
                    let loc = find_coord(&line_lens, &pos);
                    while work[point_pos].is_numeric() || work[point_pos] == '.' {
                        curr_token.push(work[point_pos]);
                        point_pos += 1;
                    }
                    match curr_token.contains(&'.') {
                        true => {
                            let tmp = curr_token.iter().collect::<String>().parse::<f64>();
                            if tmp.is_err() {
                                return Err(Errors::Parse(loc));
                            } 
                            let token = Token::new(Kind::Float(tmp.unwrap()), loc);
                            curr_token = Vec::new();
                            tokens_arr.push(token);                
                            
                        }
                        false => {
                            let tmp = curr_token.iter().collect::<String>().parse::<i64>();
                            if tmp.is_err() {
                                return Err(Errors::Parse(loc));
                            }
                            let token = Token::new(Kind::Int(tmp.unwrap()), loc);
                            curr_token = Vec::new();
                            tokens_arr.push(token);                
                            
                        }
                    }    
                }
                q if q == '\"' => {
                    let pos = point_pos;
                    let loc = find_coord(&line_lens, &pos);
                    curr_token.push(work[point_pos]);
                    point_pos += 1;
                    while work[point_pos] != '\"' {
                        curr_token.push(work[point_pos]);
                        point_pos += 1;
                        if point_pos == work_len {
                            return Err(Errors::StringClosing(loc));
                        }
                    }
                    curr_token.push(work[point_pos]);
                    let token = Token::new(Kind::Str(curr_token.iter().collect()), loc);
                    curr_token = Vec::new();
                    tokens_arr.push(token);
                    point_pos += 1;
                }
                c if c == ':' => { // Handles Property
                    let loc =find_coord(&line_lens, &point_pos);
                    while !work[point_pos].is_whitespace() && work[point_pos] != ')'{
                        curr_token.push(work[point_pos]);
                        point_pos += 1;  
                    }
                    let token = Token::new(Kind::Property(curr_token.iter().collect()),loc);
                    curr_token = Vec::new();
                    tokens_arr.push(token);
                }
                p if p == '(' => { // Handles RParen
                    let loc = find_coord(&line_lens, &point_pos);
                    point_pos += 1;  
                    tokens_arr.push(Token::new(Kind::LParen, loc));
                }
                p if p == ')' => { // Handles LParen
                    let loc = find_coord(&line_lens, &point_pos);
                    point_pos += 1;  
                    tokens_arr.push(Token::new(Kind::RParen, loc));
                } 
                w if w.is_whitespace() => { // Handles Whitespace
                    let pos = point_pos;
                    let loc = find_coord(&line_lens, &pos);
                    while work[point_pos].is_whitespace() && point_pos < work_len {
                        point_pos += 1;
                        if point_pos == work_len {break;}
                    }
                    let token = Token::new(Kind::Space,loc);
                    curr_token = Vec::new();
                    tokens_arr.push(token);
                }
                _ => { // Handles Symbol
                    let pos = point_pos;
                    let loc = find_coord(&line_lens, &pos);
                    while !work[point_pos].is_whitespace() && work[point_pos] != ')'{
                        curr_token.push(work[point_pos]);
                        point_pos += 1;  
                    }
                    match curr_token.iter().collect::<String>().as_ref() {
                        "def" => {
                            let token = Token::new(Kind::Define,loc);
                            curr_token = Vec::new();
                            tokens_arr.push(token);
                        }
                        "+" => {
                            let token = Token::new(Kind::OpPlus,loc);
                            curr_token = Vec::new();
                            tokens_arr.push(token);
                        }
                        "-" => {
                            let token = Token::new(Kind::OpMinus,loc);
                            curr_token = Vec::new();
                            tokens_arr.push(token);
                        }
                        "*" => {
                            let token = Token::new(Kind::OpMul,loc);
                            curr_token = Vec::new();
                            tokens_arr.push(token);
                        }
                        "/" => {
                            let token = Token::new(Kind::OpDiv,loc);
                            curr_token = Vec::new();
                            tokens_arr.push(token);
                        }
                        "print" => {
                            let token = Token::new(Kind::OpPrint,loc);
                            curr_token = Vec::new();
                            tokens_arr.push(token);
                        }
                        "fn" => {
                            let token = Token::new(Kind::Symbol("fn".to_owned()),loc);
                            curr_token = Vec::new();
                            tokens_arr.push(token);
                        }
                        "tailrec" => {
                            let token = Token::new(Kind::OpTailRec,loc);
                            curr_token = Vec::new();
                            tokens_arr.push(token);
                        }
                        _ => {
                            let token = Token::new(Kind::Symbol(curr_token.iter().collect()),loc);
                            curr_token = Vec::new();
                            tokens_arr.push(token);
                        } 
                    }
                }
            }

        }
        Ok(tokens_arr)
    }

    pub fn listify(tokens: Vec<Token>, start_pos: usize) -> Result<(lis<Token>, usize),Errors> {
       let mut master_list: lis<Token> = lis::new();
       let tk_len = tokens.len();
       let mut i = start_pos;
       let mut paren_weight: i32 = 0;
       // ensure paren weight is 0
       for token in &tokens {
        if token.val == Kind::LParen {
            paren_weight += 1;
        } else if token.val == Kind::RParen {
            paren_weight -= 1;
        } 
        }
        match paren_weight {
        a if a > 0 => {
            Err(Errors::ParenDown(paren_weight))
        }
        b if b < 0 => {
            Err(Errors::ParenUp(paren_weight))
        }
        _ => 
        {
            while i < tk_len {
            match &tokens[i] {
                l if l.val == Kind::LParen => {
                    let res = listify(tokens.clone(), i + 1);
                    if let Ok((nested, replace)) = res {
                        i = replace;
                        master_list.push_back(Token::new(Kind::List(nested), l.loc))
                    } else if let Err(e) = res {
                        return Err(e);
                    }
                }
                r if r.val == Kind::RParen => {
                    return Ok((master_list, i + 1))
                  }
                a => {
                    if a.val == Kind::Space {
                        i += 1;
                    } else {
                        i += 1;
                        master_list.push_back(a.clone())
                    }
                }
            }
        }
        Ok((master_list, i + 1))  
        } 
    }
}   

pub fn convert_lambdas(lis: lis<Token>) -> Result<lis<Token>,Errors> {
    let mut master_list : lis<Token> = lis::new();
    for token in &lis {
        let mut curr_token = Err(Errors::Null);
        match &token.val {
            Kind::List(l) => master_list.push_back(Token::new(Kind::List(convert_lambdas(l.clone()).unwrap_or(lis::new())), token.loc)),
            Kind::Symbol(s) => {
                match s.as_str() {
                   "fn" => { 
                        let local = lis.clone();
                        let len = local.len();
                        if len != 3 {
                            curr_token = Err(Errors::Syntax(token.clone()))
                        } else {
                            let mut iterator = lis.clone().into_iter();
                            let _d = iterator.next();
                            let argslist = iterator.next().map(|x| {
                                return match &x.val {
                                    Kind::List(q) => q.iter().map(|y| y.clone()).collect::<Vec<Token>>(),
                                    _ => vec![x], 
                                }
                            }).unwrap_or(vec![]);
                            let body = iterator.next().map(|z| {
                                let mut tmp = lis::new();
                                tmp.push_back(z);
                                let tmp = convert_lambdas(tmp);
                                tmp.unwrap_or(lis::new())
                            }).unwrap_or(lis::new());
                           curr_token = Ok(Token::new(Kind::Func(Func::new(argslist.windows(2).into_iter().filter_map(|x| eval_property(x[0].clone(), x[1].clone())).collect(),body)),token.loc)) 
                        }
                    }
                    _ => master_list.push_back(token.clone()),
                
                }
            }
            _ => master_list.push_back(token.clone()),
        };
        match curr_token {
            Ok(o) => {
                match o.val {
                    Kind::Func(_) => {
                        master_list.push_back(o.clone());
                        break;
                    }
                    _ => master_list.push_back(o.clone()),
                }
            }
            Err(_) => continue,
        }
    } 
    Ok(master_list)
}

pub fn eval_property(t: Token, p: Token) -> Option<Properties> {
    match p.val {
        Kind::Property(a) => {
            match t.val {
                Kind::Symbol(b) => {
                match a.as_str() {
                    ":Int" => {
                        Some(Properties::Type(b, Kind::Int(0)))
                    }
                    ":UInt" => {
                        Some(Properties::Type(b, Kind::UInt(0)))
                    }
                    ":Float" => Some(Properties::Type(b, Kind::Float(0.0))),
                    ":Str" => Some(Properties::Type(b, Kind::Str("".to_string()))),
                    ":List" => Some(Properties::Type(b, Kind::List(lis::new()))),
                    ":Bool" =>  Some(Properties::Type(b, Kind::Bool(1))), 
                    ":Func" => Some(Properties::Type(b, Kind::Func(Func::null()))),
                     _ => None
                }
                    }
                _ => None,
            }
            
        }
        _ => None
    }
} 




