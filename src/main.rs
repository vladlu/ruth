mod lib;
use std::io::Write;

use crate::lib::listify;
use lib::tokenize;
use lib::convert_lambdas;


fn main() {
    let args = std::env::args().collect::<Vec<String>>();
    if args.len() == 1 {
        repl();
    } else {
        file(args);
    }
}

fn read_io() -> String {
    let mut expr = String::new();
  
    std::io::stdin().read_line(&mut expr)
      .expect("Failed to read line");
  
    expr
  }
fn repl() -> ! {
    loop {
        print!("rep> ");
        let _a = std::io::stdout().lock().flush();
        let input = read_io();
        match tokenize(input) {
            Ok(s) => {
                let s = listify(s, 0);
                match s {
                    Ok(a) => {
                        let a = match convert_lambdas(a.0) {
                            Ok(w) => w,
                            Err(e) => {
                                eprintln!("{}", e);
                                std::process::exit(1)
                            }
                        };
                        for toke in a {
                            print!("{}", toke);
                        }
                        println!()        
                    }  
                    Err(e) => {
                        println!("{}",e)
                    } 
                }
            }
            Err(e) => {
                println!();
                print!("{}",e);
                println!();
            }
        }
        
    }
}

fn file(_args: Vec<String>) {

    let f = std::fs::read_to_string("src/test.ruth");
    let contents = match f {
        Ok(s) => s,
        Err(e) => {
            eprintln!("{}", e);
            std::process::exit(1);
        }
    };
    let tokens = tokenize(contents);
    let mut tokes = Vec::new();
    if let Ok(o) = tokens {
        tokes = o;
    } else if let Err(e) = tokens {
        eprintln!("{}", e);
    }
    for toke in &tokes {
        print!("{}, ",toke);
    }
    println!();
    println!("LISTS");
    match listify(tokes, 0) {
        Ok((lists, _end)) => {
            for toke in &lists {
            print!("{}, ", toke);
            }
        }
        Err(e) => {
            println!("{}",e)
        }
    }
}
