use crate::Token;
use crate::Kind;

pub fn eval(t: Token) -> Token {
    match t.val {
        Kind::Int(val) => {
            todo!()
        }
        Kind::UInt(val) => {
            todo!()
        }
        Kind::Float(val) => {
            todo!()
        }
        Kind::Str(val) => {
            todo!()
        }
        Kind::List(list) => {
            todo!()
        } 
        Kind::Symbol(name) => {
            todo!()
        }
        Kind::Property(prop) => {
            todo!()
        }
        Kind::Func(fn_ptr) => {
            todo!()
        }
        Kind::Define => {
            todo!()
        }
        Kind::DoNotEval => {
            todo!()
        }
        Kind::OpPlus => {
            todo!()
        }
        Kind::OpMinus => {
            todo!()
        }
        _ => todo!()
    }
}
